require('dotenv').config()
const request = require("supertest");
const app = require("../app");

// it dan test itu sama aja, sumber : https://stackoverflow.com/questions/45778192/what-is-the-difference-between-it-and-test-in-jest#:~:text=The%20Jest%20docs%20state%20it%20is%20an%20alias%20of%20test.%20So%20they%20are%20exactly%20the%20same%20from%20a%20functional%20point%20of%20view.%20They%20exist%20both%20to%20enable%20to%20make%20a%20readable%20English%20sentence%20from%20your%20test

describe("Car controller", () => {

    let AKSES, akses;

    beforeAll(() => {
    AKSES = request(app)
      .post("/v1/auth/login")
      .send({
        email: "johnny@binar.co.id",
        password: "123456"
      })
      .then((res) => {
        console.log(res.body.accessToken, "INI AKSES TOKEENNNNN")
        return res.body.accessToken;
      });

      akses = request(app)
      .post("/v1/auth/login")
      .send({
        email: "jansen@mail.com",
        password: "090900"
      })
      .then((res) => {
        console.log(res.body.accessToken, "ini akses tokeennnnn")
        return res.body.accessToken;
      });
    })

    // console.log(AKSES, "AKSESSSSSSSSS") // hmmm malah undefined ya ?


    it("should response with 200 as status code, if cars is available", async () => {

      return request(app)
        .get("/v1/cars")
        .then((res) => {
          expect(res.statusCode).toBe(200);
          expect(res.body).toEqual(res.body);
        });
    }, 15000);

    it("should response with 200 as status code, if cars specific id is available", async () => {
        let id = 21

        return request(app)
          .get(`/v1/cars/${id}`)
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
          });
      });


      it("should response with 201 as status code, if car successfully created and you are admin", async () => {
        const name = "RX 8 FD Stage 3 Tuned";
        const price = 900000;
        const image="https://source.unsplash.com/519x519";
        const size="SMALL";
        // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ2ODY2ODh9.p0r7HO-tC9-49FOKuYNgl4Ys_nxV_pOuKLyYpjC004w"

        let data = {
            name : name,
            price : price,
            image : image,
            size : size
        }
    
        return request(app)
          .post("/v1/cars")
          .set("Content-Type", "application/json")
          .set("Authorization", `Bearer ${await AKSES}`)
          .send(data)
          .then((res) => {
            expect(res.statusCode).toBe(201);
            expect(res.body).toEqual(res.body);
            
          });
        });

      it("should response with 422 as status code, if car failed to creat car even if you are admin ", async () => {
        // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ2ODY2ODh9.p0r7HO-tC9-49FOKuYNgl4Ys_nxV_pOuKLyYpjC004w"

        return request(app)
          .post("/v1/cars")
          .set("Content-Type", "application/json")
          .set("Authorization", `Bearer ${await AKSES}`)
          .send({})
          .then((res) => {
            expect(res.statusCode).toBe(422);
            expect(res.body).toEqual(res.body);
            
          });
        });


        it("should response with 200 as status code, if car successfully updated and you are admin", async () => {
        const name = "RX 0 FC Stage 3 Tuned";
        const price = 900000;
        const image="https://source.unsplash.com/519x519";
        const size="SMALL";
        // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ2ODkxMDh9.vRcOqbaEd8VJpmTOp4QMaxTXxrUGF1Hl0varKik2LoY"
        let idMobil = 30
    
        let data = {
            name : name,
            price : price,
            image : image,
            size : size
        }
        
        return request(app)
            .put(`/v1/cars/${idMobil}`)
            .set("Authorization", `Bearer ${await AKSES}`)
            .send(data)
            .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
            });
        });

        it("should response with 422 as status code, if car failed to be updated", async () => {
          // const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ2ODkxMDh9.vRcOqbaEd8VJpmTOp4QMaxTXxrUGF1Hl0varKik2LoY"
          let idMobil = 105
      
          return request(app)
              .put(`/v1/cars/${idMobil}`)
              .set("Authorization", `Bearer ${await AKSES}`)
              .send({})
              .then((res) => {
              expect(res.statusCode).toBe(422);
              expect(res.body).toEqual(res.body);
              });
          });

        it("should response with 201 as status code, if car successfully rented and you are consumer", async () => {
            
            // let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwibmFtZSI6ImphbnNlbiIsImVtYWlsIjoiamFuc2VuQG1haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3MTA4MzF9.bFUqS5glwJHqjRvZBq_gB0MVuuLnIMDe6uvLSDVc2_Q"
            let idMobil = 50
            return request(app)
              .post(`/v1/cars/${idMobil}/rent`)
              .set("Content-Type", "application/json")
              .set("Authorization", `Bearer ${await akses}`)
              .send({
                'rentStartedAt': "2022-06-08T15:23:41.438Z",
                'rentEndedAt': "2022-06-30T15:23:41.438Z"
              })
              .then((res) => {
                expect(res.statusCode).toBe(201);
                expect(res.body).toEqual(res.body);
              });
            });

        it("should response with 422 as status code, if car is rented", async () => {
        
            // let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwibmFtZSI6ImphbnNlbiIsImVtYWlsIjoiamFuc2VuQG1haWwuY29tIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjEsIm5hbWUiOiJDVVNUT01FUiJ9LCJpYXQiOjE2NTQ3MTA4MzF9.bFUqS5glwJHqjRvZBq_gB0MVuuLnIMDe6uvLSDVc2_Q"
            let idMobil = 50;
            return request(app)
                .post(`/v1/cars/${idMobil}/rent`)
                .set("Content-Type", "application/json")
                .set("Authorization", `Bearer ${await akses}`)
                .send({
                  'rentStartedAt': "2022-06-08T15:23:41.438Z",
                  'rentEndedAt': "2022-06-25T15:23:41.438Z"
                })
                .then((res) => {
                expect(res.statusCode).toBe(422);
                expect(res.body).toEqual(res.body);
                });
            });    

        it("should response with 204 as status code, if car successfully deleted", async () => {
            let idMobil = 20
            // let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ2ODkxMDh9.vRcOqbaEd8VJpmTOp4QMaxTXxrUGF1Hl0varKik2LoY"
            
            return request(app)
                .delete(`/v1/cars/${idMobil}`)
                .set("Authorization", `Bearer ${await AKSES}`)
                .then((res) => {
                expect(res.statusCode).toBe(204);
                expect(res.body).toEqual(res.body);
                });
            });  

            it("should response with 404 as status code, if car with id doesnt exist", async () => {
              let idMobil = 9999999999
              let token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwibmFtZSI6IkpvaG5ueSIsImVtYWlsIjoiam9obm55QGJpbmFyLmNvLmlkIiwiaW1hZ2UiOm51bGwsInJvbGUiOnsiaWQiOjIsIm5hbWUiOiJBRE1JTiJ9LCJpYXQiOjE2NTQ3NzQ2MjB9.8OO5MeYj3vf4SxAF6N6UDdjE-5kHRY8LZ8rDzte8qpc"

              return request(app)
                  .delete(`/v1/cars/${idMobil}`)
                  .set("Authorization", `Bearer ${token}`)
                  .then((res) => {
                  expect(res.statusCode).toBe(404);
                  expect(res.body).toEqual(res.body);
                  });
              });  


});