require('dotenv').config()
const request = require("supertest");
const app = require("../app");

describe("Application controller", () => {

    it("should response with 404 as status code, if endpoint is unavailable", async () => {

      return request(app)
        .get("/asdasdasdasdasd")
        .then((res) => {
          expect(res.statusCode).toBe(404);
          expect(res.body).toEqual(res.body);
        });
    });

    it("should response with 200 as status code, if endpoint is /", async () => {

        return request(app)
          .get("/")
          .then((res) => {
            expect(res.statusCode).toBe(200);
            expect(res.body).toEqual(res.body);
          });
      });

    // Ngetest nya gimana .. ?
    // it("should response with 500 as status code, kalau internal server error ( misal server belum jalan )", async () => {

    // return request(app)
    //     .get("")
    //     .then((res) => {
    //     expect(res.statusCode).toBe(500);
    //     expect(res.body).toEqual(res.body);
    //     });
    // });
  
  });
  